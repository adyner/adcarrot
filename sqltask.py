
import task;

class SqlTask(Task):
	def __init__(self, id, name, conn, sql_text):
		Task.__init__(self, id, name);
		self.__connection = conn;
		self.__sql_text = sql_text;
	
	def execute():
		print("Start execute sql")
		self.__conn.execute(self.__sql_text);
		print("Finish execute sql")
