
import threading

class TaskThread(threading.Thread):
	def __init__(self, task, callback):
		self.__task = task;
		self.__on_finished_callbacks = [];
	
	def subscribe_on_finished(self, callback):
		self.__on_finished_callbacks.append(callback);
	
	def run(self):
		print("start execute task " + self.__task.Id);
		self.__task.execute();
		print("finsih execute task " + self.__task.Id);
		for c in self.__on_finished_callbacks:
			c(self.__task.Id)
		
