
from tasksheduler import TaskSheduler
from sqltask import SqlTask

sheduler = TaskSheduler()

conn1 = None
task11 = SqlTask("t11", "t11", conn, "update dev42_db_stg.t_coa where session_inst_id = -session_inst_id")
task12 = SqlTask("t12", "t12", conn, "update dev42_db_stg.t_coa where session_inst_id = -session_inst_id")

sheduler.add_to_playlist(task11)
sheduler.add_to_playlist(task12)
sheduler.add_task_prerequisite(task12, task11)


conn2 = None
task21 = SqlTask("t21", "t21", conn, "update dev42_db_stg.t_je_line where session_inst_id = -session_inst_id")
task22 = SqlTask("t22", "t22", conn, "update dev42_db_stg.t_je_line where session_inst_id = -session_inst_id")

sheduler.add_to_playlist(task21)
sheduler.add_to_playlist(task22)
sheduler.add_task_prerequisite(task22, task21)

sheduler.play();

print("Finish work")
