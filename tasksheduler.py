
import threading


class TaskSheduler():
	def __init__(self):
		self.__threads = []
		self.__tasks = {}
		self.__deps = {}
		self.__task_prerequisite_info = {}
		self.__lock = threading.RLock()
		
	def add_to_playlist(self, task):
		self.__tasks[task.Id] = task;
		self.__deps[task.Id] = []
		self.__task_prerequisite_info[task.Id] = [0, 0]
	
	def add_task_prerequisite(self, child_task, parent_task)
		self.__deps[parent_task.Id].append(child_task)
		info = self.__task_prerequisite_info[child_task.Id]
		info[0] = info[0] + 1
	
	def play(self):
		print("Play")
		for task_id, task_info in self.__task_prerequisite_info.items():
			if task_info[0] == 0:
				self.__start_task():
				
		for t in self.__threads:
			t.join()
		print("Finish work")
	
	def __start_task(self, task_id):
		print("start task " + task_id)
		executor = TaskThread(self.__tasks[task_id]);
		executor.subscribe_on_finished(self.__notify_on_finish);
		executor.start();
		self.__threads.append(executor);
	
	def __notify_on_finish(self, task_id):
		self.__lock.acquire()
		for child_task in self.__deps[task_id]:
			info = self.__task_prerequisite_info[child_task.Id]
			info[1] = info[1] + 1
			if info[0] == info[1]:
				self.__start_task(child_task.Id);
		self.__lock.release()
		
